# /usr/bin/python3
# -*- coding: utf-8 -*-
# Clase (y programa principal) para un proxy

import os
import sys
import hashlib
import json
import socket
import socketserver
from datetime import datetime, date, time, timedelta
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

ATT = {'server': ['name', 'ip', 'puerto'],
       'database': ['path', 'passwdpath'],
       'log': ['path']}

USAGE_ERROR = 'Usage: python3 proxy_registrar.py config'
EJECUTAR_AUDIO = "./mp32rtp -i ip -p port < audio"

SIP_MESSAGE = {'100': 'SIP/2.0 100 Trying\r\n\r\n',
               '180': 'SIP/2.0 180 Ringing\r\n\r\n',
               '200': 'SIP/2.0 200 OK\r\n\r\n',
               '400': 'SIP/2.0 400 Bad Request\r\n\r\n',
               '401': 'SIP/2.0 401 Unauthorized\r\n\r\n',
               '404': 'SIP/2.0 404 User Not Found\r\n\r\n',
               '405': 'SIP/2.0 405 Method Not Allowed\r\n\r\n'}


def get_digest_nonce(server_name, ip):
    d_nonce = hashlib.sha512()
    d_nonce.update(bytes(server_name + ip, "utf-8"))
    d_nonce.digest()
    return d_nonce.hexdigest()


def get_digest_response(nonce, username, passwd):
    d_nonce = hashlib.sha512()
    d_nonce.update(bytes(nonce + username + passwd, "utf-8"))
    d_nonce.digest()
    return d_nonce.hexdigest()


class XMLHandler(ContentHandler):
    # Clase para el proxy

    def __init__(self, ATT):
    	# Declaramos la lista y las etiquetas de TAGS
        self.data = {}
        self.ATT = ATT

    def startElement(self, name, attrs):
    	# Añadimos los atributos a las etiquetas y metemos en la lista
        if name in self.ATT:
            for Attributes in self.ATT[name]:
                self.data[name + '_' + Attributes] = attrs.get(Attributes, '')

    def get_tags(self):
    	# Devuelve una lista con las etiquetas y sus atributos
        return self.data


class LOG:
    # Escribiendo en el archivo log

    def __init__(self, path):
        if not os.path.exists(path):
            os.system('touch ' + path)
        self.path = path

    def write(self, msg):
        with open (self.path, 'a') as log_file:
            now = datetime.now().strftime('%Y%m%d%H%M%S')
            log_file.write(now + '' + msg)

    def starting(self):
        msg = 'Starting...\n'
        self.write(msg)

    def error(self, error_mess):
        msg = 'Error: ' + error_mess + '\n'
        self.write(msg)

    def sent_to(self, ip, port, mess):
        m = mess.replace('\r\n', '')
        msg = 'Sent to ' + ip + ':' + str(port) + ':' + m + '\n'
        self.write(msg)

    def received_from(self, ip, port, mess):
        m = mess.replace('\r\n', '')
        msg = 'Received from  ' + ip + ':' + str(port) + ':' + m + '\n'
        self.write(msg)

    def finishing(self):
        msg = 'Finishing...\n'
        self.write(msg)


class SIPHandler(socketserver.DatagramRequestHandler):
    # SIP Register

    diccdata = {}
    passwd = {}

    def expires_time(self):
    	# Eliminar un usuario
        user_del = []
        now = datetime.now().strftime('%H:%M:%S %d-%m-%Y')
        try:
            for user in self.diccdata:
                if timenow >= self.diccdata[user]['expires']:
                    user_del.append(user)
            for user in user_del:
                del self.diccdata[user]
        except:
            pass

    def register2json(self):
    	# Convierto el diccionario a json
        with open(config['database_path'], 'w') as jsonfile:
            json.dump(self.diccdata, jsonfile, indent=4)

    def json2registered(self):
    	# Lee un archivo json y usa la informacion como un diccionario de usuarios
        self.expires_time()
        try:
            with open(config['database_path'], 'r') as jsonfile:
                self.diccdata = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def json2passwd(self):
    	# Abre un archivo y usa la informacion como un diccionario de usuarios
    	# y contraseñas
        try:
            with open(config['database_passwdpath'], 'r') as jsonfile:
                self.passwd = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def sent_to(self, user, mess):
    	# Envio de informacion a traves del socket
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            ip = self.diccdata[user]['address'].split(':')[0]
            port = self.diccdata[user]['address'].split(':')[1]
            my_socket.connect((ip, int(port)))
            my_socket.send(bytes(mess, 'utf-8'))
            try:
                data = my_socket.recv(1024).decode('utf-8')
                log.received_from(ip, port, data)
            except:
                data = ''
                error_mess = 'No server listening at ' + ip + ' port ' + \
                             str(port)
                log.error(error_mess)
        return data

    def handle(self):
        # Metodo Handle

        self.json2registered()
        self.json2passwd()

        the_message = self.rfile.read().decode('utf-8')
        IP_client = self.client_address[0]
        PORT_client = str(self.client_address[1])
        log.received_from(IP_client, PORT_client, the_message)

        METHOD = the_message.split()[0]
        print(METHOD, 'received')

        if METHOD == 'REGISTER' or METHOD == 'register':
            user = the_message.split()[1].split(':')[1]
            if user in self.diccdata:
                expired = the_message.split('\n')[1].split(':')[1]
                if int(expired) == 0:
                    del self.diccdata[user]
                    self.wfile.write(bytes(SIP_MESSAGE['200'], 'utf-8'))
                    log.sent_to(IP_client, PORT_client, SIP_MESSAGE['200'])
                    print('user ' + user + ' log out')
                else:
                    exp_time = datetime.now() + timedelta(seconds=int(expired))
                    exp_date = exp_time.strftime('%H:%M:%S %d-%m-%Y')
                    self.diccdata[user]['expires'] = exp_date
                    self.wfile.write(bytes(SIP_MESSAGE['200'], 'utf-8'))
                    log.sent_to(IP_client, PORT_client, SIP_MESSAGE['200'])
                    print('user ' + user + ' log in')
            else:
                if 'Authorization' in the_message:
                    resp_user = the_message.split('\n')[2].split('"')[1]
                    server_name = config['server_name']
                    server_ip = config['server_ip']
                    nonce = get_digest_nonce(server_name, server_ip)
                    username = the_message.split('\n')[0].split(':')[1]
                    passwd = self.passwd[username]
                    response = get_digest_response(nonce, username, passwd)
                    if resp_user == response:
                        expires = the_message.split('\n')[1].split(':')[1]
                        ip = self.client_address[0]
                        port = the_message.split()[1].split(':')[2]
                        address = ip + ':' + port
                        now = datetime.now()
                        exp_time = now + timedelta(seconds=int(expires))
                        exp_date = exp_time.strftime('%H:%M:%S %d-%m-%Y')
                        self.diccdata[user] = {'address': address,
                                               'expires': exp_date}
                        self.wfile.write(bytes(SIP_MESSAGE['200'], 'utf-8'))
                        log.sent_to(IP_client, PORT_client, SIP_MESSAGE['200'])
                    else:
                        self.wfile.write(bytes(SIP_MESSAGE['400'], 'utf-8'))
                        log.sent_to(ADDRESS, SIP_MESSAGE['400'])
                else:
                    server_name = config['server_name']
                    server_ip = config['server_ip']
                    nonce = get_digest_nonce(server_name, server_ip)
                    line = SIP_MESSAGE['401'].split('\r\n\r\n')[0] + '\r\n'
                    line += 'WWW Authenticate: Digest nonce="'
                    line += nonce + '"\r\n'
                    self.wfile.write(bytes(line, 'utf-8'))
                    log.sent_to(IP_client, PORT_client, line)

        elif METHOD == 'INVITE' or METHOD == 'invite':
            user_src = the_message.split('\r\n')[4].split()[0].split('=')[1]
            user_dst = the_message.split()[1].split(':')[1]
            if user_src in self.diccdata and user_dst in self.diccdata:
                mess = self.sent_to(user_dst, the_message)
                self.wfile.write(bytes(mess, 'utf-8'))
                log.sent_to(IP_client, PORT_client, mess)
            else:
                self.wfile.write(bytes(SIP_MESSAGE['404'], 'utf-8'))
                log.sent_to(IP_client, PORT_client, SIP_MESSAGE['404'])

        elif METHOD == 'ACK' or METHOD == 'ack':
            user_dst = the_message.split()[1].split(':')[1]
            if user_dst in self.diccdata:
                mess = self.sent_to(user_dst, the_message)
                self.wfile.write(bytes(mess, 'utf-8'))
                log.sent_to(IP_client, PORT_client, mess)

        elif METHOD == 'BYE' or METHOD == 'bye':
            user_dst = the_message.split()[1].split(':')[1]
            if user_dst in self.diccdata:
                mess = self.sent_to(user_dst, the_message)
                self.wfile.write(bytes(mess, 'utf-8'))
                log.sent_to(IP_client, PORT_client, mess)
            else:
                self.wfile.write(bytes(SIP_MESSAGE['404'], 'utf-8'))
                log.sent_to(IP_client, PORT_client, SIP_MESSAGE['404'])
        else:
            self.wfile.write(bytes(SIP_MESSAGE['405'], 'utf-8'))
            log.sent_to(IP_client, PORT_client, SIP_MESSAGE['405'])
        self.register2json()


if __name__ == '__main__':
    # Listen at localhost ('') port given
    # and calls the SIPHandler class to manage the request
    if len(sys.argv) != 2:
        sys.exit(USAGE_ERROR)
    else:
        if os.path.exists(sys.argv[1]):
            xml_file = sys.argv[1]
        else:
            sys.exit('file ' + sys.argv[1] + ' not found')

    parser = make_parser()
    Handler = XMLHandler(ATT)
    parser.setContentHandler(Handler)
    parser.parse(open(xml_file))
    config = Handler.get_tags()

    IP_SERV = config['server_ip']
    PORT_SERV = int(config['server_puerto'])
    serv = socketserver.UDPServer((IP_SERV, PORT_SERV), SIPHandler)
    log = LOG(config['log_path'])

    print('Server ' + IP_SERV + ' listening at ' + str(PORT_SERV) + '...\r\n')
    try:
        log.starting()
        serv.serve_forever()
    except KeyboardInterrupt:
        log.finishing()
        print("End Server")
