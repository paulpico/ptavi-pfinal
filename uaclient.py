# /usr/bin/python3
# -*- coding: utf-8 -*-
# Programa cliente que abre un socket a un servidor

import socket
import sys
import os
from datetime import datetime
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from proxy_registrar import XMLHandler, LOG, get_digest_response

ATT = {'account': ['username', 'passwd'],
       'uaserver': ['ip', 'puerto'],
       'rtpaudio': ['puerto'],
       'regproxy': ['ip', 'puerto'],
       'log': ['path'],
       'audio': ['path']}

USAGE_ERROR = 'Usage: python3 uaclient.py config method option'
EJECUTAR_AUDIO = "./mp32rtp -i ip -p port < audio"
Methods_Allowed = ['register', 'invite', 'bye', 'ack']


class SIPMessages:
    #Clase para los metodos SIP

    def __init__(self, xml_file):
        # Funcion Init
        parser = make_parser()
        Handler = XMLHandler(ATT)
        parser.setContentHandler(Handler)
        parser.parse(open(xml_file))
        self.config = Handler.get_tags()

    def register(self, option, Auth=''):
        # Funcion de Registro
        USER = self.config['account_username']
        PORT = int(self.config['uaserver_puerto'])
        Message = 'REGISTER sip:' + USER + ':' + str(PORT) + ' SIP/2.0'
        Message += '\r\n' + 'Expires: ' + option
        if Auth != '':
            Message += '\r\nAuthorization: Digest response="' + Auth + '"'
        return Message + '\r\n'

    def invite(self, option):
        # Funcion de Invitacion
        USER = self.config['account_username']
        SERVER = self.config['uaserver_ip']
        PORT_AUDIO = self.config['rtpaudio_puerto']
        Message = 'INVITE sip:' + option + ' SIP/2.0\r\n'
        Message += 'Content-Type: application/sdp\r\n\r\nv=0\r\no='
        Message += USER + ' ' + SERVER + '\r\ns=misesion\r\nt=0\r\n'
        Message += 'm=audio ' + PORT_AUDIO + ' RTP\r\n'
        return Message

    def bye(self, option):
        # Funcion Bye
        Message = 'BYE sip:' + option + ' SIP/2.0\r\n'
        return Message

    def ack(self, option):
        # Funcion Ack
        Message = 'ACK sip:' + option + ' SIP/2.0\r\n'
        return Message

    def send(self, socket, method, option, Auth=''):
        USER = self.config['account_username']
        if method.lower() in Methods_Allowed:
            if method.lower() == 'register':
                m = self.register(option, Auth)
            elif method.lower() == 'invite':
                m = self.invite(option)
            elif method.lower() == 'bye':
                m = self.bye(option)
            elif method.lower() == 'ack':
                m = self.ack(option)
        else:
            m = method.upper() + ' sip:' + USER + ' SIP/2.0\r\n'

        print("Enviando:\n" + m)
        log.sent_to(PR_SERV, PR_PORT, m)
        socket.send(bytes(m, 'utf-8'))

    def receive(self, socket):
        try:
            data = socket.recv(1024).decode('utf-8')
            log.received_from(PR_SERV, PR_PORT, data)
        except:
            data = ''
            log.error('No server listening at ' + PR_SERV + ' port ' + str(PR_PORT))
        return data

    def get_mess(self, method, option, Auth=''):
        if method.lower() == 'register':
            return self.register(option, Auth)
        elif method.lower() == 'invite':
            return self.invite(option)
        elif method.lower() == 'bye':
            return self.bye(option)
        elif method.lower() == 'ack':
            return self.ack(option)


def trying_ringing_ok(data):
    trying = '100' in data
    ringing = '180' in data
    ok = '200' in data

    return trying and ringing and ok


if __name__ == '__main__':
    if len(sys.argv) != 4:
        raise SystemExit(USAGE_ERROR)
    else:
        if os.path.exists(sys.argv[1]):
            xml_file = sys.argv[1]
            method = sys.argv[2]
            option = sys.argv[3]
        else:
            raise SystemExit('file ' + sys.argv[1] + ' not found')

    client = SIPMessages(xml_file)
    log = LOG(client.config['log_path'])
    log.starting()

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        PR_SERV = client.config['regproxy_ip']
        PR_PORT = int(client.config['regproxy_puerto'])
        my_socket.connect((PR_SERV, PR_PORT))
        client.send(my_socket, method, option)
        data = client.receive(my_socket)
        print('Recibido:\n' + data)

        if 'SIP/2.0 401 Unauthorized' in data:
            print(data.split('\r\n'))
            nonce = data.split('\r\n')[1].split('"')[1]
            user = client.config['account_username']
            passwd = client.config['account_passwd']
            response = get_digest_response(nonce, user, passwd)
            client.send(my_socket, method, option, response)
            data = client.receive(my_socket)
            print('Recibido:\n' + data)

        elif trying_ringing_ok(data):
            sdp = data.split('\r\n')[-7:-1]
            ip = sdp[1].split()[1]
            port = sdp[-2].split()[1]
            audio = client.config['audio_path']
            client.send(my_socket, 'ack', option)
            mp32rtp = EJECUTAR_AUDIO.replace('ip', ip).replace('port', port)
            mp32rtp = mp32rtp.replace('audio', audio)
            print('enviando audio a', ip + ':' + port + '...')
            os.system(mp32rtp)

    log.finishing()
