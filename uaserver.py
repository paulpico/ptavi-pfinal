# /usr/bin/python3
# -*- coding: utf-8 -*-
# Clase (y programa principal) para un server

import sys
import os
import time
import socketserver
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from proxy_registrar import XMLHandler, LOG, SIP_MESSAGE

USAGE_ERROR = 'Usage: python3 uaserver.py config'
ERROR_ARG = 'Introduce un número de argumentos válidos'
EJECUTAR_AUDIO = "./mp32rtp -i ip -p port < audio"

ATT = {'account': ['username', 'passwd'],
       'uaserver': ['ip', 'puerto'],
       'rtpaudio': ['puerto'],
       'regproxy': ['ip', 'puerto'],
       'log': ['path'],
       'audio': ['path']}


class SHandler(socketserver.DatagramRequestHandler):
    #Clase para el Servidor

    mp32rtp = []

    def handle(self):
    	#Funcion del handle
        the_message = self.rfile.read().decode('utf-8')
        ip = self.client_address[0]
        port = str(self.client_address[1])
        log.received_from(ip, port, the_message)
        METHOD = the_message.split()[0]
        print(METHOD, 'received')

        if METHOD == 'INVITE' or METHOD == 'invite':
            sdp = the_message.split('\r\n')[1:]
            sdp_body = ''
            for line in sdp:
                if 'o=' in line:
                    user = line.split('=')[1].split()[0]
                    ip = line.split('=')[1].split()[1]
                    self.mp32rtp.append(ip)
                    new_line = line.replace(user, config['account_username'])
                    new_line = new_line.replace(ip, config['uaserver_ip'])
                    sdp_body += new_line + '\r\n'
                elif 'm=' in line:
                    rtpaudio = line.split()[1]
                    self.mp32rtp.append(rtpaudio)
                    rtpport = config['rtpaudio_puerto']
                    sdp_body += line.replace(rtpaudio, rtpport) + '\r\n'
                else:
                    sdp_body += line + '\r\n'
            self.wfile.write(bytes(SIP_MESSAGE['100'], 'utf-8'))
            log.sent_to(ip, port, SIP_MESSAGE['100'])
            self.wfile.write(bytes(SIP_MESSAGE['180'], 'utf-8'))
            log.sent_to(ip, port, SIP_MESSAGE['180'])
            self.wfile.write(bytes(SIP_MESSAGE['200'] + sdp_body, 'utf-8'))
            log.sent_to(ip, port, SIP_MESSAGE['200'] + sdp_body)

        elif METHOD == 'ACK' or METHOD == 'ack':
            command = EJECUTAR_AUDIO.replace('ip', self.mp32rtp[0])
            command = command.replace('port', self.mp32rtp[1])
            command = command.replace('audio', config['audio_path'])
            print('enviando audio a', self.mp32rtp[0] + ':' + self.mp32rtp[1])
            os.system(command)

        elif METHOD == 'BYE' or METHOD == 'bye':
            self.wfile.write(bytes(SIP_MESSAGE['200'], 'utf-8'))
            log.sent_to(ip, port, SIP_MESSAGE['200'])

        else:
            self.wfile.write(bytes(SIP_MESSAGE['405'], 'utf-8'))
            log.sent_to(ip, port, SIP_MESSAGE['405'])


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print(ERROR_ARG)
        raise SystemExit(USAGE_ERROR)
    else:
        if os.path.exists(sys.argv[1]):
            xml_file = sys.argv[1]
        else:
            raise SystemExit('file ' + sys.argv[1] + ' not found')

    parser = make_parser()
    Handler = XMLHandler(ATT)
    parser.setContentHandler(Handler)
    parser.parse(open(xml_file))
    config = Handler.get_tags()

    IP_SERV = config['uaserver_ip']
    PORT_SERV = int(config['uaserver_puerto'])
    serv = socketserver.UDPServer((IP_SERV, PORT_SERV), SHandler)
    log = LOG(config['log_path'])

    print('Escuchando en la direccion: ' + IP_SERV + ' ' + str(PORT_SERV) + '...\r\n')
    try:
        log.starting()
        serv.serve_forever()
    except KeyboardInterrupt:
        log.finishing()
        print("End Server")
